import Vue from 'vue';
import App from './App.vue';

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import VueLazyload from 'vue-lazyload'
import VModal from 'vue-js-modal'

Vue.use(VModal)
Vue.use(Buefy)
Vue.config.productionTip = true;
Vue.use(VueLazyload, {
    lazyComponent: true
})
new Vue({
    render: h => h(App)

}).$mount('#app');