﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoApp.Backend.Models
{
    public class MyImage
    {
        public string[] Base64Strings { get; set; }
        public bool LastPack { get; set; }
    }
}
