﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Nancy;

namespace PhotoApp.Backend.Helper
{
    public class PhotoHelper
    {
        public static string ImageToBase64(string name)
        {
            byte[] imageArray = File.ReadAllBytes($"{name}");
            return Convert.ToBase64String(imageArray);
        }

        public static string GeneratePhotoName(int length, string valid)
        {
            var res = new StringBuilder();
            using (var rng = new RNGCryptoServiceProvider())
            {
                var uintBuffer = new byte[sizeof(uint)];

                while (length-- > 0)
                {
                    rng.GetBytes(uintBuffer);
                    var num = BitConverter.ToUInt32(uintBuffer, 0);
                    res.Append(valid[(int)(num % (uint)valid.Length)]);
                }
            }

            //if name exist, restart method
            if (File.Exists($"Images/{res}.jpg"))
                GeneratePhotoName(20, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");

            return $"{res}.jpg";
        }

        public static void SaveFullImage(HttpFile file, string fullName)
        {
            using var fileStream = File.OpenWrite(fullName);
            file.Value.CopyTo(fileStream);
            fileStream.Close();
        }

        public static void SavePreviewImage(string fullName, string previewName)
        {
            using var fS2 = File.OpenRead(fullName);
            var inputImage = Image.FromStream(fS2);
            //create image with 10% size

            Bitmap outputImage;

            outputImage = inputImage.Width >= 200
                ? PhotoHelper.ResizeImage(inputImage, 200, (int)(inputImage.Height * (200.0 / inputImage.Width)))
                : PhotoHelper.ResizeImage(inputImage, inputImage.Width, inputImage.Height);
            fS2.Close();
            outputImage.Save(previewName);
        }

        //From this: https://stackoverflow.com/questions/1922040/how-to-resize-an-image-c-sharp/24199315
        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
    }
}
