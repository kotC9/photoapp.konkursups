﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoApp.Backend.Helper
{
    public class Global
    {
        public static string PreviewImageFolder = @"Images/Preview";
        public static string ImagesFolder = @"Images";
        public static string FullSizeImageFolder = @"Images/Full-Size";
    }
}
