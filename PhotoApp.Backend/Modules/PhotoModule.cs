﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using PhotoApp.Backend.Helper;
using PhotoApp.Backend.Models;

namespace PhotoApp.Backend.Modules
{
    public sealed class PhotoModule : BaseModule
    {
        public PhotoModule()
        {
            Get("/get-preview-images/{startindex:int}", args =>
            {
                Console.WriteLine("get preview images");
                //return 20 preview images
                var startIndex = args.startindex;

                var endIndex = Directory.GetFiles(Global.PreviewImageFolder).Length > startIndex + 20
                    ? (int)(startIndex + 20)
                    : Directory.GetFiles(Global.PreviewImageFolder).Length;

                var previewImages = new List<string>();
                var i = (int)args.startindex;

                if (i >= endIndex) return HttpStatusCode.NotFound;

                for (i = args.startindex; i < endIndex; i++)
                    //convert to base64 because it's faster for small images
                    if (File.Exists($@"{Global.PreviewImageFolder}/{i}.jpg"))
                        previewImages.Add(PhotoHelper.ImageToBase64($@"{Global.PreviewImageFolder}/{i}.jpg"));

                return Response.AsJson(previewImages);
            });

            Get("/get-image-by-index/{sindex}", args =>
            {
                Console.WriteLine("get image by index");
                var index = args.sindex;
                return File.Exists($@"{Global.PreviewImageFolder}/{index}.jpg") && File.Exists($@"{Global.FullSizeImageFolder}/{index}.jpg")
                    //base64 потому что не все файлы возвращаются нормально через Response.AsFIle();
                    ? Response.AsJson(PhotoHelper.ImageToBase64($@"{Global.FullSizeImageFolder}/{index}.jpg"))
                    : HttpStatusCode.NotFound;
            });

            Post("/send-images", args =>
            {
                Console.WriteLine("send images");
                var files = Request.Files.ToArray();

                foreach (var file in files)
                {
                    //var newName = PhotoHelper.GeneratePhotoName(20, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");

                    /* only for this project.
                     * names is 0.jpg 1.jpg 2.jpg 3.jpg etc (easy way to save preview images on server)
                     * but the best option to create preview pictures - is on the client side
                     * */
                    var lastPhoto = File.Exists($@"{Global.FullSizeImageFolder}/0.jpg")
                        ? Directory.GetFiles(Global.FullSizeImageFolder).Length
                        : 0;

                    var fullName = $@"{Global.FullSizeImageFolder}/{lastPhoto}.jpg";
                    PhotoHelper.SaveFullImage(file, fullName);
                }

                var firstFile = File.Exists($@"{Global.PreviewImageFolder}/0.jpg")
                    ? Directory.GetFiles(Global.PreviewImageFolder).Length
                    : 0;

                var lastFileAgain = File.Exists($@"{Global.FullSizeImageFolder}/0.jpg")
                    ? Directory.GetFiles(Global.FullSizeImageFolder).Length
                    : 0;

                for (var i = firstFile; i < lastFileAgain; i++)
                {
                    //create and save preview image
                    var fullName = $@"{Global.FullSizeImageFolder}/{i}.jpg";
                    var previewName = $@"{Global.PreviewImageFolder}/{i}.jpg";
                    PhotoHelper.SavePreviewImage(fullName, previewName);
                }

                return HttpStatusCode.OK;
            });
        }
    }
}
