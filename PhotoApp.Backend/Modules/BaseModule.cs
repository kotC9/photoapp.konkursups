﻿using System;
using System.Collections.Generic;
using System.Text;
using Nancy;

namespace PhotoApp.Backend.Modules
{
    public abstract class BaseModule : NancyModule
    {
        protected BaseModule(string path = null)
        {
            ModulePath = string.IsNullOrEmpty(path)
                ? "api"
                : "api/" + path;
        }
    }
}
