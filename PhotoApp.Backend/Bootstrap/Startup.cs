﻿using System;
using System.Collections.Generic;
using System.Text;
using Nancy.Hosting.Self;
using PhotoApp.Backend.Helper;

namespace PhotoApp.Backend.Bootstrap
{
    class Startup
    {
        static void Main(string[] args)
        {

            if (!System.IO.Directory.Exists(Global.ImagesFolder))
                System.IO.Directory.CreateDirectory(Global.ImagesFolder);

            if (!System.IO.Directory.Exists(Global.PreviewImageFolder))
                System.IO.Directory.CreateDirectory(Global.PreviewImageFolder);

            if (!System.IO.Directory.Exists(Global.FullSizeImageFolder))
                System.IO.Directory.CreateDirectory(Global.FullSizeImageFolder);

            var bindUrl = new Uri("http://localhost:8081");
            var bootstrapper = new Bootstrapper();
            var configuration = new HostConfiguration()
            {
                UrlReservations =
                {
                    CreateAutomatically = true
                }
            };

            using (var host = new NancyHost(bootstrapper, configuration, bindUrl))
            {
                host.Start();
                Console.WriteLine("NancyFX Stand alone test application.");
                Console.WriteLine("Press enter to exit the application");
                Console.ReadLine();
            }
        }
    }
}
