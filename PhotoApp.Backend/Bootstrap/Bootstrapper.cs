﻿using System;
using System.Collections.Generic;
using System.Text;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

namespace PhotoApp.Backend.Bootstrap
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            AllowAccessToConsumingSite(pipelines);
        }

        private static void AllowAccessToConsumingSite(IPipelines pipelines)
        {
            pipelines.AfterRequest.AddItemToEndOfPipeline(x =>
            {
                {
                    x.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                    x.Response.Headers.Add("Access-Control-Allow-Methods", "POST,GET,DELETE,PUT,OPTIONS");
                    x.Response.Headers.Add("Access-Control-Expose-Headers",
                        "Cache-Control, Content-Language, Content-Type, Expires, Last-Modified, X-Suggested-Filename, Pragma");
                    x.Response.Headers.Add("Access-Control-Allow-Headers",
                        "Accept, Origin, Authorization, Content-type, Cache-control, X-Requested-With");
                }
            });
        }
    }
}
